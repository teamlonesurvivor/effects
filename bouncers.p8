pico-8 cartridge // http://www.pico-8.com
version 32
__lua__
gravity=0.92
rndcolors={{2,13,14},{13,14,15},{3,11,10},{1,12,7},{8,9,10}}
bouncers={}

function _init()
 add(bouncers,new_bouncer(rnd(127),flr(rnd(20))-20,0.25,1,5))
end

function _draw()
 cls(0)
 for b in all(bouncers) do
  b.draw()
 end
// print(#bouncers,16,16,7)
end

function _update()
 for b in all(bouncers) do
  b.update()
  if b.candelete then
   del(bouncers,b)
  end
 end
 if #bouncers==0 then
  add(bouncers,new_bouncer(64,0,flr(rnd(3))-2,0,rnd(10)))
 end
end
-->8
function new_bouncer(x,y,xspd,yspd,r)
 local b={x=x,y=y,r=r,candelete=false,candeletein=-1}
 local yspeed=yspd
 local xspeed=xspd
 local exploding=false
 local explodingsize=b.r+flr(rnd(12))
 //local col=rndcolors[flr(rnd(#rndcolors))+1]
 local col=rndcolors[3]
 local tail={}
 
 b.explode=function(this)
  b.candeletein=0.1
  if #bouncers<50 then
   for i=1,flr(rnd(4)) do
    local xs=flr(rnd(20))-10
    local ys=-flr(rnd(10))
    if flr(rnd(100))<12 then
     ys=-15
    end
    add(bouncers,new_bouncer(b.x,b.y,xs,ys,3))
   end
  end
  exploding=true
 end
 
 b.hitwall=function(this)
  if b.x-(b.r*0.5)<0 or b.x+(b.r*0.5)>127 then
   return true
  end
  return false
 end
 
 b.hitground=function(this)
  if b.y+(b.r*0.5) >= 127-(b.r*0.5) then
   return true
  end
  return false
 end
 
 local lastx=b.x
 local lasty=b.y
 
 b.update=function(this)
  if #tail>10 then
   deli(tail,1)
  end
  b.candeletein-=0.1
  if b.candeletein==0 then
   b.candelete=true
   b.exploding=false
  end
  if b.hitwall() then
   xspeed=-xspeed
  end
  if b.hitground() then
   yspeed=-yspeed+gravity
   b.y=127-b.r
  end
  if yspeed<=1 and yspeed>=-1 and b.hitground() then
   yspeed=0
   xspeed=0
   b.y=127-b.r
  end
  if b.hitground() and xspeed==0 and yspeed==0 then
   b.r+=1
   if b.r >= explodingsize then
    b.explode()
   end
  end

  yspeed+=gravity
  b.y+=yspeed
  b.x+=xspeed
  for t in all(tail) do
   t.y-=1
   t.r-=1
  end
  add(tail,{x=b.x,y=b.y,r=b.r})
  lastx=b.x
  lasty=b.y
 end
 
 b.draw=function(this)
  for t in all(tail) do
   circfill(t.x,t.y,t.r,col[1])
  end
  if exploding then
   circfill(b.x,b.y,b.r+rnd(15),7)
   circ(b.x,b.y,b.r+rnd(20),7)
   line(b.x-(b.r+explodingsize*0.5),b.y-1,b.x+(b.r+explodingsize*0.5),b.y-1,7)
   line(b.x-(b.r+explodingsize),b.y,b.x+(b.r+explodingsize),b.y,7)
   line(b.x-(b.r+explodingsize*0.5),b.y+1,b.x+(b.r+explodingsize*0.5),b.y+1,7)
   line(b.x-1,b.y-(b.r+explodingsize*0.5),b.x-1,b.y+(b.r+explodingsize*0.5),7)
   line(b.x,b.y-(b.r+explodingsize),b.x,b.y+(b.r+explodingsize),7)
   line(b.x+1,b.y-(b.r+explodingsize*0.5),b.x+1,b.y+(b.r+explodingsize*0.5),7)
  end
  
  circfill(b.x,b.y,b.r,col[1])
  circfill(b.x,b.y,b.r-1,col[2])
  //print(#tail,b.x,b.y,7)
 end
 
 return b
end

__gfx__
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00700700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00077000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00077000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00700700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
