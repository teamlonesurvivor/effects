pico-8 cartridge // http://www.pico-8.com
version 32
__lua__
function _init()
 poke(0x5f2d,1)
 effects={}
 colors={{11,7},{10,7},{12,7},{8,7}}
end

function _update()
 for e in all(effects) do
  e.update()
  if e.candelete then
   del(effects,e)
  end
 end
 cursorx=stat(32)-1
 cursory=stat(33)-1
 if stat(34)==1 and not mouseclick then
  mouseclick=true
  flash=true
  local col=colors[flr(rnd(#colors))+1]
  assert(col!=nil,col[1])
  add(effects,new_flash(cursorx,cursory,rnd(48)+16,col))
 elseif stat(34)==0 then
  mouseclick=false
 end

end

function _draw()
 cls(0)
 for e in all(effects) do
  e.draw()
 end
 circfill(cursorx,cursory,1,7)
end
-->8
function new_flash(x,y,maxr,cols)
 local f={x=x,y=y,candelete=false}
 local r=3
 local retracting=false
 local flashing=true
 f.update=function(this)
  if retracting then
   r-=24
   if r<=0 then
    f.candelete=true
   end
  else
   r+=24
   if r>maxr then
    retracting=true
   end
  end
 end
 f.draw=function(this)
  if flashing then
   cls(7)
   flashing=false
  end
  circfill(x,y,r*0.5,cols[1])
  circfill(x,y,r*0.33,cols[2])
  line(x-(r*0.75),y+1,x+(r*0.75),y+1,cols[2])
  line(x-r,y,x+r,y,cols[2])
  line(x-(r*0.75),y-1,x+(r*0.75),y-1,cols[2])
 end
 return f
end
__gfx__
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00700700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00077000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00077000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00700700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
